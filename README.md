    1: Clone repository: git clone https://AteqEjaz@bitbucket.org/AteqEjaz/sam-challenge.git
    2: cd sam-challenge
    3: rails s (default port 3000)

now use post man to hit followign API with POST method "http://localhost:3000/api/v1/request_format/check_response_format"
you will need to send format either Json or XML in header.

XML body example =>
 <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:hs="http://www.holidaywebservice.com/HolidayService_v2/">
 <soapenv:Body>
     <hs:GetHolidaysForMonth>
        <hs:year>2018</hs:year>
        <hs:countryCode>UnitedStates</hs:countryCode>
        <hs:month>11</hs:month>
     </hs:GetHolidaysForMonth>
 </soapenv:Body>
</soapenv:Envelope>

JSON body example
	{
	"card_number":"4141748739629471",
	"month": "05",
	"year": "2022"
	}

how to send request? Please watch this video(https://drive.google.com/file/d/18WIcnim1bBvYpgmm_2IWS-2gyu8RMO8L/view)
thanks
