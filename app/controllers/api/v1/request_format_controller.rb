require 'active_support'
require 'json'
require 'active_support/core_ext'
require 'nokogiri'

class Api::V1::RequestFormatController < ApplicationController

  def check_response_format

    if request.content_type.eql?("application/xml")
      xml_body = Nokogiri::XML(request.body)
      json_response = Hash.from_xml(xml_body.to_s).to_json rescue nil
      return json_response('error', 'No XML found in request body') unless json_response
      json_response('result', json_response)


    elsif request.content_type.eql?("application/json")
      json_array = []

      params.each do |key, value|
        next if key.eql?('controller') or key.eql?('action') or key.eql?('format') or key.eql?('request_format')
        hash = {"#{key}": value}
        json_array << hash
      end

      return json_response('error', 'No parameter found in request body' ) if json_array.empty?
      xml_response = {xml: json_array}.to_xml
      render xml: xml_response

    else
      render json: {error: "Please send Json or XML request."}
    end
  end

  def json_response(status, message)
    render json: {"#{status}": message}
  end

end